// Import de la bibliothèque Ownable d'OpenZeppelin
import "@openzeppelin/contracts/access/Ownable.sol";

// Déclaration du contrat Voting
contract Voting is Ownable {
    // Structure de données pour représenter un électeur
    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    // Structure de données pour représenter une proposition
    struct Proposal {
        string description;
        uint voteCount;
    }

    // Énumération pour gérer les différents états du vote
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    // Événements
    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId);

    // Variables de contrat
    WorkflowStatus public workflowStatus;
    uint public winningProposalId;

    // Mapping pour stocker les électeurs
    mapping(address => Voter) public voters;

    // Tableau dynamique de propositions
    Proposal[] public proposals;

    // Modifier pour restreindre les actions à certaines phases du vote
    modifier onlyAtState(WorkflowStatus _targetStatus) {
        require(workflowStatus == _targetStatus, "Action non autorisee a cet etat.");
        _;
    }

    // Constructeur du contrat
    constructor() {
        workflowStatus = WorkflowStatus.RegisteringVoters;
    }

    // Fonction pour inscrire un électeur
    function registerVoter(address _voterAddress) public onlyOwner onlyAtState(WorkflowStatus.RegisteringVoters) {
        require(!voters[_voterAddress].isRegistered, "L electeur est deja inscrit.");
        voters[_voterAddress].isRegistered = true;
        emit VoterRegistered(_voterAddress);
    }

    // Fonction pour démarrer la session d'enregistrement des propositions
    function startProposalsRegistration() public onlyOwner onlyAtState(WorkflowStatus.RegisteringVoters) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, WorkflowStatus.ProposalsRegistrationStarted);
    }

    // Fonction pour clôturer la session d'enregistrement des propositions
    function endProposalsRegistration() public onlyOwner onlyAtState(WorkflowStatus.ProposalsRegistrationStarted) {
        workflowStatus = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, WorkflowStatus.ProposalsRegistrationEnded);
    }

    // Fonction pour enregistrer une proposition
    function registerProposal(string memory _description) public onlyAtState(WorkflowStatus.ProposalsRegistrationStarted) {
        uint proposalId = proposals.length;
        proposals.push(Proposal(_description, 0));
        emit ProposalRegistered(proposalId);
    }

    // Fonction pour démarrer la session de vote
    function startVotingSession() public onlyOwner onlyAtState(WorkflowStatus.ProposalsRegistrationEnded) {
        workflowStatus = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, WorkflowStatus.VotingSessionStarted);
    }

    // Fonction pour clôturer la session de vote
    function endVotingSession() public onlyOwner onlyAtState(WorkflowStatus.VotingSessionStarted) {
        workflowStatus = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, WorkflowStatus.VotingSessionEnded);
    }

    // Fonction pour voter
    function vote(uint _proposalId) public onlyAtState(WorkflowStatus.VotingSessionStarted) {
        require(voters[msg.sender].isRegistered, "Vous n etes pas inscrit pour voter.");
        require(!voters[msg.sender].hasVoted, "Vous avez deja vote.");
        require(_proposalId < proposals.length, "Proposition invalide.");
        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = _proposalId;
        proposals[_proposalId].voteCount++;
        emit Voted(msg.sender, _proposalId);
    }

    // Fonction pour comptabiliser les votes et déterminer le gagnant
    function tallyVotes() public onlyOwner onlyAtState(WorkflowStatus.VotingSessionEnded) {
        uint winningCount = 0;
        uint winningProposal = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningCount) {
                winningCount = proposals[i].voteCount;
                winningProposal = i;
            }
        }
        winningProposalId = winningProposal;
        workflowStatus = WorkflowStatus.VotesTallied;
    }

    // Fonction pour obtenir le gagnant
    function getWinner() public view returns (uint) {
        require(workflowStatus == WorkflowStatus.VotesTallied, "Les votes n'ont pas encore ete comptabilises.");
        return winningProposalId;
    }
}
