import React from "react";

function WorkflowStatus({ status }) {
  return (
    <div className="workflow-status">
      <p>État du contrat : {status}</p>
    </div>
  );
}

export default WorkflowStatus;