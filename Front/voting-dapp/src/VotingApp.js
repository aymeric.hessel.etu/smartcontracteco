import React, { useState, useEffect } from "react";
import { ethers } from "ethers";
import VotingContract from "./contracts/Voting.json";

function VotingApp() {
  const [contract, setContract] = useState(null);
  const [account, setAccount] = useState("");
  const [workflowStatus, setWorkflowStatus] = useState(0); // Utilisez l'enum pour les valeurs d'état
  const [proposals, setProposals] = useState([]);
  const [selectedProposal, setSelectedProposal] = useState("");

  useEffect(() => {
    async function initialize() {
      // Connexion au contrat
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const networkId = await provider.getNetwork();
      const contractAddress = VotingContract.networks[networkId.chainId].address;
      const votingContract = new ethers.Contract(contractAddress, VotingContract.abi, signer);

      setContract(votingContract);

      // Obtenez le compte connecté
      const accounts = await window.ethereum.request({ method: "eth_accounts" });
      setAccount(accounts[0]);
    }

    initialize();
  }, []);

  const startProposalsRegistration = async () => {
    await contract.startProposalsRegistration();
  };

  const registerProposal = async () => {
    await contract.registerProposal(selectedProposal);
  };

  const startVotingSession = async () => {
    await contract.startVotingSession();
  };

  const vote = async (proposalId) => {
    await contract.vote(proposalId);
  };

  return (
    <div className="voting-app">
      <h1>Contrat de Vote</h1>
      <p>Compte connecté : {account}</p>
      <button onClick={startProposalsRegistration}>Démarrer l'enregistrement des propositions</button>
      <button onClick={startVotingSession}>Démarrer la session de vote</button>
      {workflowStatus === 2 && (
        <div>
          <input
            type="text"
            placeholder="Nouvelle proposition"
            value={selectedProposal}
            onChange={(e) => setSelectedProposal(e.target.value)}
          />
          <button onClick={registerProposal}>Enregistrer la proposition</button>
        </div>
      )}
      {workflowStatus === 4 && (
        <div>
          <h2>Propositions enregistrées :</h2>
          <ul>
            {proposals.map((proposal, index) => (
              <li key={index}>
                {proposal.description} - Votes : {proposal.voteCount}
                <button onClick={() => vote(index)}>Voter</button>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default VotingApp;